package controle;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class Conexao {
	private Connection con;
	
	public Connection abrirConexao() {
		this.con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			this.con = DriverManager.getConnection("jdbc:mysql://localhost/javaWeb", "root", "123456");
		}catch(ClassNotFoundException e) {
			System.out.println("Arquivo de biblioteca não encontrado");
		}catch(SQLException e) {
			System.out.println("Problema ao conectar ao banco: " + e.getMessage());
		}
		return this.con;
	}
	public boolean fecharConexao(Connection con) {
		boolean resultado = false;
		try {
			con.close();
			resultado = true;
		}catch(SQLException e) {
			System.out.println("Não foi possível fechar a conexão: " + e.getMessage());
		}
		return resultado;
	}	
}