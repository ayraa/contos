package controle;
import modelo.Contos;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;
public class ContosControle {
	public boolean logar(Login inst) {
		boolean resultado = false;
		try {
		Connection con = new Conexao().abrirConexao();
		String sql = "SELECT * FROM login WHERE nome=? and senha=?";
		PreparedStatement ps = con.prepareStatement(sql,ResultSet.TYPE_SCROLL_INSENSITIVE, 
				ResultSet.CONCUR_READ_ONLY);
		ps.setString(1, inst.getNome());
		ps.setString(2, inst.getSenha());
		ResultSet rs = ps.executeQuery();
		if(rs != null && rs.next()) {
			Login usuario = new Login();
			usuario.setNome(rs.getString("nome"));
			usuario.setSenha(rs.getString("senha"));
			resultado = true;		
		}
		new Conexao().fechar(con);
    	}catch(SQLException e) {
    			e.getMessage();
    	}
    		return resultado;
    }

	public ArrayList<Contos> consultarTodos(){
		ArrayList<Contos> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM contos;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList<Contos>();
				while(rs.next()) {
					Contos contos = new Contos(rs.getInt("id"),rs.getString("titulo"),rs.getString("texto"),rs.getString("imagem"));
					lista.add(contos);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	public boolean add(Contos contos) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("Insert INTO contos(titulo,texto,imagem) VALUES (?,?,?)");
			ps.setString(1, contos.getTitulo());
			ps.setString(2, contos.getTexto());
			ps.setString(3, contos.getImagem());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao adicionar conto." + e.getMessage());
		}
		return resultado;
	}
	
	public boolean editar(Contos contos) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE contos SET titulo=?, texto=?, imagem=? WHERE id=?");
			ps.setString(1,contos.getTitulo());
			ps.setString(2, contos.getTexto());
			ps.setString(3, contos.getImagem());
			ps.setString(4, contos.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar conto: " + e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro: "+ e.getMessage());
		}
		return resultado;
	}

	public boolean remover(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM contos WHERE id=?");
			ps.setInt(1, id);
			if(ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return resultado;
	}
	
}
