package beans;

import javax.faces.bean.ManagedBean;
import modelo.Contos;
import controle.ContosControle;

@ManagedBean(name="ContosBean")
public class ContosBean{
	int id;
	String titulo, texto, imagem;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}
	public String getImagem() {
		return texto;
	}

	public void setImagem(String texto) {
		this.imagem = imagem;
	}
	
	public String add() {
		Contos user = new Contos(this.getId(),this.getTitulo(), this.getTexto(), this.getImagem());
		if(new ContosControle().add(contos)) {
			return "index";
		}else {
			return "inserir";
		}
	}
	public String remover(int id) {
		new ContosControle().remover(id);
		return "index";
	}
	public String editar() {
		Contos user = new Contos(this.getId(), this.getTitulo(), this.getTexto(), this.getImagem());
		if(new ContosControle().editar(contos)) {
			return "index";	
		}else {
			return "editar";
		}
	}



}
